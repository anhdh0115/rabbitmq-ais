# from msilib.schema import Class
from operator import le
import pika
import datetime
import os
import json
from scapy.layers.l2 import Ether
from scapy.all import *
from DataExtractorModule import DataExtractorModule
import pandas as pd
import timeit
from Classifier import NetworkClassifier
import base64
data_extractor = DataExtractorModule()
import config
class Server():
    def __init__(self):
        self.model_dl = NetworkClassifier()
        
        self.credentials = pika.PlainCredentials(
                    # 'user', 'password'
                    os.getenv("RABBITMQ_DEFAULT_USER"), os.getenv("RABBITMQ_DEFAULT_PASS")
                    )
        self.host = os.getenv("ip") or "14.232.152.36"
        # print(host)
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                    host=self.host, 
                    credentials=self.credentials))
        while True:
            try:
                self.connection = pika.BlockingConnection(
                    pika.ConnectionParameters(host=self.host, credentials=self.credentials))
                print("\n\nconnect to rabbitMQ sucessfull\n\n")
                print(self.host)
                print(self.credentials)
            except Exception:
                print("Connect to RabbitMQ Error, attemp reconnect..")
                print(self.host)
                print(self.credentials)
                continue
            break
        self.channel = self.connection.channel()

        self.channel.exchange_declare(exchange=config.RABBITMQ_CONFIG_NETWORKING['exchange'],
                                      exchange_type='direct', durable=True)
        # queue của agent thường
        # self.result = self.channel.queue_declare(
        #     queue=config.RABBITMQ_CONFIG_NETWORKING['queue'], durable=True, arguments=config.RABBITMQ_CONFIG_NETWORKING['queue_arguments'])
        # logger.info(self.result)
        # self.queue_name = self.result.method.queue

        self.channel.queue_bind(
            exchange=config.RABBITMQ_CONFIG_NETWORKING['exchange'], queue=config.RABBITMQ_CONFIG_NETWORKING['queue'], routing_key=config.RABBITMQ_CONFIG_NETWORKING['routing_key'])

        self.channel.basic_consume(
            queue=config.RABBITMQ_CONFIG_NETWORKING['queue'], on_message_callback=self.callback, auto_ack=True)  
        
        self.channel.start_consuming()

    def callback(self, ch, method, properties, body):
        data = body.decode('utf-8')
        self.extractor = DataExtractorModule()
        data = data.replace('{"device_id":', '')
        data = data.replace("}","")
        data = data.replace("'","")
        list_data = list(data.split(', "message":b'))
        self.device_id = list_data[0]
        # print(list_data[1][0:10])
        self.pkt = Ether(list_data[1].encode())
        # print()
        self.pcap_path = f'output_put_pcap/network{datetime.now().isoformat()}.pcap'
        wrpcap(self.pcap_path, self.pkt)
        # output = data_extractor.extract_pcap(pcap_path)
        self.handle_pcap_data()

    def start_consuming(self):        
        self.channel.start_consuming()

    def stop_consuming(self):
        self.channel.stop_consuming()

    def handle_pcap_data(self):
        output = self.extractor.extract_pcap(
            self.pcap_path)
        print(0000000000000000000000000000)
        if output:
            pcap_data = pd.read_csv(output).loc[:, 'src_ip':'dst_ip']
            pcap_data.drop_duplicates(inplace=True)
            pcap_data.sort_values(
                by='src_ip', inplace=True, ignore_index=True)
            print(pcap_data)
            # self.req.add_dst_ip(self.device_id, list(
            #     pcap_data['dst_ip'].values))
            # TODO
            begin_to_check_n_load = timeit.timeit()
            done_check_n_load = timeit.timeit()
            print("\n\ncheck n load model = %r\n\n" %
                        (abs(done_check_n_load-begin_to_check_n_load)))
            ret = self.model_dl.predict(output)
            if not ret.empty:
                print(
                    'Detected SUSPICIOUS network flows:\n' + str(ret))
                # try:
                #     # TODO
                #     self.req.add_network_alert(
                #         self.device_id, ' '.join(ret['pred'].values))
                # except Exception as e:
                #     print(e)
            else:
                print('Network traffic is normal')
                pass
 
            try:
                os.remove(output)
            except Exception as removeOutputCsvException:
                print(removeOutputCsvException)
        print("im done with pcaps")
        pass
