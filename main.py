import sentry_sdk
import sys


# from IoTAnalyzerConsumer import IoTAnalyzerConsumer
from Server import Server
from _thread import start_new_thread
from signal import signal, SIGINT
# from loadbalancing.InformationStrategy import InformationStrategy
# from KafkaBinaryConsumer import PilotDistributedConsumer
# from kafka import KafkaProducer
import coloredlogs
import logging
import os
# from dotenv import load_dotenv, find_dotenv
from pathlib import Path
# from ELK import ELK
from socket import gethostname
from dotenv import load_dotenv, find_dotenv
sys.path.append('.')
from Server import Server
# from Request2Django import Request2Django
import logging.handlers

# from tasks import validate_db

# coloredlogs.install(level='INFO')

# req = Request2Django(os.getenv("django_ip"))


class RequestsHandler(logging.Handler):
    def emit(self, record):
        log_entry = self.format(record)
        # req.add_bug_alert(os.getenv("django_ip"), log_entry)


def main():
    def handler(signal_received, frame):
        logging.info('SIGINT or CTRL-C detected. Server closed')
        consumerIoTAnalyzer.stop_consuming()  # Stop consumer
        exit(0)
    signal(SIGINT, handler)
    # while True:
    consumerIoTAnalyzer = Server()
    start_new_thread(consumerIoTAnalyzer.start_consuming, ())
    # server = Server('', 2020)


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    customHandler = RequestsHandler()
    customHandler.setLevel(logging.ERROR)
    logger.addHandler(customHandler)

    Path("./log").mkdir(parents=True, exist_ok=True)
    load_dotenv(find_dotenv())

    # Validate trạng thái database
    # validate_db(os.getenv("django_ip"))
    main()
