import socket
import logging
import coloredlogs
import ipaddress
import subprocess
import os
import json
import pandas as pd
import timeit
import shutil
from shutil import rmtree
from multiprocessing import cpu_count
from joblib import Parallel, delayed, parallel_backend
from _thread import start_new_thread
from datetime import datetime, time
from constants import *
from sys import getsizeof
logger = logging.getLogger(__name__)
from cicflowmeter import sniffer

class DataExtractorModule():
    def __init__(self):
        # self.logger = logger
        # self.req = req
        # self.device_id = 
        self.output_folder = f'log/'
        self.counter = 0
        self.limit = 100
        self.csv = pd.DataFrame()
        # self.center_server = Request2Django(os.getenv("center_ip"), True)
        try:
            if os.path.exists(self.output_folder):
                rmtree(self.output_folder, ignore_errors=True)
            os.mkdir(self.output_folder)
        except Exception as e:
            logger.error(e)

        self.BANDWIDTH_UP = 0
        self.BANDWIDTH_DOWN = 0
        self.CPU_IDLE = 0
        self.CPU_TOTAL = 0

    # def run(self, data, timestamp, device_id, info='batch', **kwargs):
    #     def extract(i):
    #         if i == 1:
    #             return self.stat(data.get(str(i), b''))
    #         elif i == 2:
    #             return self.mem(data.get(str(i), b''))
    #         elif i == 3:
    #             return self.proc(data.get(str(i), b''))
    #         elif 4 <= i <= 7:
    #             return self.opening_port(data.get(str(i), b''), i)
    #         elif i == 8:
    #             return self.bandwidth(data.get(str(i), b''))
    #         elif i == 9:
    #             return self.syslog(data.get(str(i), b''))
    #         elif i == 10:
    #             return self.get_mac(data.get(str(i), b''))
    #         elif i == 11:
    #             return {'integrity': data.get(str(i), b'').decode()}
    #         # TODO nếu i = 11 thì extract data phần check tính toàn vẹn.
    #     start_data_extraction = timeit.timeit()
    #     output = []
    #     if info == 'batch':
    #         with parallel_backend('threading', n_jobs=2):
    #             output = Parallel(n_jobs=2, prefer='threads')(
    #                 delayed(extract)(i) for i in range(1, 12))
    #         ts = timestamp
    #         # logger.info(output)
    #         # data_elk = {"stat": output[0], "mem": output[1], "proc": output[2],
    #         # "opening_port": output[3:7], "bandwidth": output[7], "syslog": output[8], "get_mac": output[9], "integrity": output[10]}
    #         try:
    #             # kwargs['elk'].push_one_document(
    #             #     'stats', {**data_elk, **{"host": self.output_folder}})
    #             # update to db: ProcessList of  this device
    #             # self.logger.info(output)
    #             # self.logger.info(output)
    #             OPENING_PORT = output[3:7]
    #             CPU_TOTAL = output[0]["CPU_user"] + output[0]["CPU_nice"] + output[0]["CPU_system"] + output[0]["CPU_idle"] + output[0]["CPU_iowait"] + \
    #                 output[0]["CPU_irq"] + output[0]["CPU_softirq"] + output[0]["CPU_steal"] + \
    #                 output[0]["CPU_guest"] + output[0]["CPU_guest_nice"]
    #             CPU_IDLE = output[0]['CPU_idle']
    #             PERCENT_CPU = float("{:0.2f}".format(
    #                 100 - (CPU_IDLE - self.CPU_IDLE)/(CPU_TOTAL - self.CPU_TOTAL)*100))
    #             self.CPU_IDLE = CPU_IDLE
    #             self.CPU_TOTAL = CPU_TOTAL

    #             RAM_TOTAL = int(output[1]['MemTotal'])
    #             RAM_FREE = int(output[1]['MemFree'])
    #             RAM_BUFFERS = int(output[1]['Buffers'])
    #             RAM_CACHED = int(output[1]['Cached'])
    #             PERCENT_RAM = float("{:0.2f}".format(
    #                 (RAM_TOTAL - RAM_FREE - RAM_BUFFERS - RAM_CACHED) / RAM_TOTAL * 100))

    #             BANDWIDTH_UP = 0
    #             BANDWIDTH_DOWN = 0
    #             for bandwitdh in output[7]:
    #                 receive_bytes = int(bandwitdh['Receive bytes'])
    #                 transmit_bytes = int(bandwitdh['Transmit bytes'])

    #                 BANDWIDTH_DOWN = BANDWIDTH_DOWN + receive_bytes
    #                 BANDWIDTH_UP = BANDWIDTH_UP + transmit_bytes

    #             SPEED_UP = float("{:0.2f}".format(
    #                 abs(BANDWIDTH_UP - self.BANDWIDTH_UP)/1024))
    #             SPEED_DOWN = float("{:0.2f}".format(
    #                 abs(BANDWIDTH_DOWN - self.BANDWIDTH_DOWN)/1024))

    #             self.BANDWIDTH_UP = BANDWIDTH_UP
    #             self.BANDWIDTH_DOWN = BANDWIDTH_DOWN

    #             self.req.add_process_list(device_id, output[2])

    #             # self.logger.info(ts)
    #             self.req.update_device_by_id(device_id, {"resource_info": {
    #                 'PERCENT_RAM': PERCENT_RAM, 'PERCENT_CPU': PERCENT_CPU, 'SPEED_UP': SPEED_UP, 'SPEED_DOWN': SPEED_DOWN, 'timestamps': ts, "OPENING_PORT": OPENING_PORT}})
    #         except Exception as e:
    #             self.logger.error(e)
    #         with open(f'{self.output_folder}/batch.json', 'w') as f:
    #             json.dump(output, f)
    #         # self.logger.info(f'Extracted info batch')
    #     elif info == 'pcap':
    #         output = self.extract_pcap(data)
    #     stop_data_extraction = timeit.timeit()
    #     logger.warning("total data extraction time = %r " %
    #                    (abs(stop_data_extraction - start_data_extraction)))
    #     return output

    # def stat(self, data):
    #     if not data:
    #         return {}
    #     output = {}
    #     cpu_key = CPU_INFO
    #     lines = data.split(b'\n')[:-1]
    #     lines = [line.decode('utf-8') for line in lines]
    #     for line in lines:
    #         if 'cpu ' in line:
    #             value = line.split()[1:]
    #             for k, v in zip(cpu_key, value):
    #                 output['CPU_' + k] = int(v)
    #         elif 'procs_' in line:
    #             value = line.split()
    #             output[value[0]] = int(value[1])
    #         else:
    #             value = line.split()
    #             if value[0].isnumeric():
    #                 output['uptime'] = float(lines[0].split()[0])
    #     return output

    # def mem(self, data):
    #     if not data:
    #         return {}
    #     output = {}
    #     lines = data.split(b'\n')[:-1]
    #     lines = [line.decode('utf-8') for line in lines]
    #     for i in range(len(lines)):
    #         line = lines[i].split()
    #         k, v = line[0][:-1], int(line[1])
    #         output[k] = v
    #     return output

    # def proc(self, data):
    #     if not data:
    #         return []
    #     output = []
    #     stat = PROC_INFO
    #     lines = data.split(b'\n')[:-1]
    #     lines = [line.decode('utf-8') for line in lines]
    #     i = 0
    #     while i < len(lines):
    #         row = dict()
    #         value = lines[i].split()
    #         row['pid'] = int(value[0])
    #         if len(value) > 2:
    #             row['comm'] = ''
    #             j = 1
    #             while True:
    #                 row['comm'] += value[j] + ' '
    #                 j += 1
    #                 if ')' in value[j-1]:
    #                     break
    #             for k, v in zip(stat, value[j:]):
    #                 if k in PROC_INFO_NEEDED:
    #                     try:
    #                         row[k] = v if k in [
    #                             'comm', 'state'] else int(v)
    #                     except:
    #                         return
    #             i += 1
    #             if i < len(lines):
    #                 value = lines[i].split()
    #         if len(value) == 2:
    #             row['sha1'] = value[1]
    #             i += 1
    #         output.append(row)
    #     return output

    # def opening_port(self, data, i):
    #     def get_addr(v):
    #         hex_ip, port = v.split(':')
    #         port = int(port, 16)
    #         if len(hex_ip) == 8:
    #             ip = ipaddress.IPv4Network(int(hex_ip, 16))
    #         else:
    #             ip = ipaddress.IPv6Network(int(hex_ip, 16))

    #         ip = str(ip).split('/')[0]
    #         ip = '.'.join(reversed(ip.split('.')))
    #         return ip, port

    #     if not data:
    #         return []
    #     output = []
    #     lines = data.split(b'\n')[: -1]
    #     lines = [line.decode('utf-8') for line in lines]
    #     key = PORT_INFO
    #     state = PORT_STATE
    #     proto = ''
    #     if i == 4:
    #         proto = 'tcp'
    #     elif i == 5:
    #         proto = 'tcp6'
    #     elif i == 6:
    #         proto = 'udp'
    #     elif i == 7:
    #         proto = 'udp6'

    #     for line in lines:
    #         if key[1] not in line:
    #             row = dict({'protocol': proto})
    #             value = line.split()
    #             for k, v in zip(key, value):
    #                 if 'address' in k:
    #                     ip, p = get_addr(v)
    #                     v = f'{ip}:{p}'
    #                     if k == 'local_address':
    #                         row['port'] = int(p)
    #                     row[k] = v
    #                 elif k == 'st':
    #                     row['state'] = state.get(v, '')
    #             output.append(row)
    #     return output

    # def bandwidth(self, data):
    #     if not data:
    #         return []
    #     output = []
    #     lines = data.split(b'\n')[: -1]
    #     lines = [line.decode('utf-8') for line in lines]
    #     for line in lines:
    #         if not line.startswith('Inter-') and not line.startswith(' face'):
    #             value = line.split()
    #             row = dict()
    #             row['Interface'] = value[0][: -1]
    #             row['Receive bytes'] = value[1]
    #             row['Receive packets'] = value[2]
    #             row['Transmit bytes'] = value[9]
    #             row['Transmit packets'] = value[10]
    #             output.append(row)
    #     return output

    # def syslog(self, data):
    #     if not data:
    #         return []
    #     output = []
    #     lines = data.split(b'\n')[: -1]
    #     lines = [line.decode('utf-8') for line in lines]
    #     for line in lines:
    #         now = datetime.utcnow().isoformat()
    #         output.append({
    #             '@timestamp': now,
    #             'message': line
    #         })
    #     return output

    def extract_cic(self, pcap_path):
        output_path = f'{self.output_folder}/cic.csv'
        if os.path.exists(output_path):
            os.remove(output_path)
        # cmd = f'cicflowmeter -f {pcap_path} -c {output_path}'
        # p = subprocess.Popen(
        #     cmd, shell=True)
        # p.wait()
        s = sniffer.create_sniffer(
            input_interface=None,
            input_file=pcap_path,
            output_file=output_path,
            output_mode="flow"
        )
        s.start()
        try:
            s.join()
        except Exception as e:
            logger.error(e)
            s.stop()
        finally:
            s.join()
        if os.path.exists(output_path) and os.stat(output_path).st_size:
            df = pd.read_csv(output_path)
            self.csv = pd.concat([self.csv, df])
            self.counter += 1
            logger.info(self.counter)
            if self.counter == self.limit:
                # self.center_server.upload_csv_to_center_server(self.csv)
                # hadoop - not tested
                self.csv.to_csv(output_path+"batch.csv")
                self.counter = 0
                self.csv = pd.DataFrame()
            return output_path
        else:
            return None

    def extract_pcap(self, pcap_path):
        # if not data:
        #     return []

        # pcap_path = f'{self.output_folder}/network{datetime.now().isoformat()}.pcap'
        pcapfix_path = 'tools/pcapfix/pcapfix'
        # dest_extracted_dir = './server/pcap_extracted'

        # with open(pcap_path, 'wb') as f:
        #     f.write(data)

        cmd = f'{pcapfix_path} -o {pcap_path} {pcap_path}'
        print(cmd)
        p = subprocess.Popen(
            cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.wait()

        # Extract files from pcap file

        output = self.extract_cic(pcap_path)
        # extract_pcap.delay(pcap_path)

        # pcap_path_to_snort = f'data/network{datetime.now().isoformat()}.pcap'
        # file_pcap = '../'+pcap_path_to_snort
        # print(pcap_path)
        # print(pcap_path_to_snort)
        # os.rename(pcap_path, pcap_path_to_snort)
        # extract_pcap.delay(file_pcap)
        # pretreatment_file_upload_aws.delay()
        return output

    def get_mac(self, data):
        return {'MAC': data[:-2].decode('utf-8').split()}
